package com.yaoby.gw2.bosstimer;

public interface MyContext {
	public enum WorldServerEnum {  
		CHINA_TELECOM_1(1,"电信一区"), CHINA_TELECOM_2(2,"电信二区"), CHINA_UNICOM_1(3,"联通一区");
		
		int id;
		String name;
		
		WorldServerEnum(int id,String name){
			this.id =id;
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
	}  
}
