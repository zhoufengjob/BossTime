package com.yaoby.gw2.bosstimer.adapter;

import java.util.ArrayList;//
import java.util.Calendar;
import java.util.Date;

import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.response.bosstime.BossTime;
import com.yaoby.gw2.bosstimer.utils.DateTimeUtil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BossTimeAdapter extends BaseAdapter {
//
	private ArrayList<BossTime> bossList;
	private Context context;
	private ArrayList<String[]> cacheBossTimeList;
	private int[] bossImgs;

	public BossTimeAdapter(Context context, ArrayList<BossTime> bossList) {
		this.bossList = bossList;
		this.context = context;
		getBossTime(bossList);
		
		bossImgs = new int[] {
				R.drawable.boss1,
				R.drawable.boss2,
				R.drawable.boss3,
				R.drawable.boss4,
				R.drawable.boss5,
				R.drawable.boss6,
				R.drawable.boss7,
				R.drawable.boss8,
				R.drawable.boss9,
				R.drawable.boss10,
				R.drawable.boss11,
				R.drawable.boss12,
				R.drawable.boss13
				};

	}

	/**
	 * 计算boss时间
	 * @param allbossList
	 */
	private void getBossTime(ArrayList<BossTime> allbossList) {
		for (int position = 0; position < allbossList.size(); position++) {

			// boss所有刷新时间
			ArrayList<String> bosstimelist = bossList.get(position).getTime();

			Calendar bossDate_cal1 = Calendar.getInstance();
			Calendar androidDate_cal2 = Calendar.getInstance();

			Date androidDate = new Date();

			for (int i = 0; i < bosstimelist.size(); i++) {
				// 补全时间
				String bossTime = DateTimeUtil.getDate() + " " + bosstimelist.get(i);
				Date bossdate = DateTimeUtil.parse(bossTime, "");

				bossDate_cal1.setTime(bossdate); // 设置boss时间
				androidDate_cal2.setTime(androidDate); // 设置本机时间

				// 如果boss时间在当前手机时间之后
				if (bossDate_cal1.after(androidDate_cal2)) {
					String bossRefreshTimer = bossList.get(position).getTime().get(i);
					String bossLastRefresh = bossList.get(position).getTime().get(i - 1 < 0 ? 0 : i - 1);
					String bossNextRefresh = bossList.get(position).getTime().get(i);

//					holder.tv_bossTime.setText(bossRefreshTimer);
//					holder.tv_bossTime_last.setText("上次次刷新\n" + bossLastRefresh);
//					holder.tv_bossTime_next.setText("下次刷新\n" + bossNextRefresh);

					if (cacheBossTimeList == null) {
						cacheBossTimeList = new ArrayList<String[]>();
					}

					cacheBossTimeList.add(new String[] { bossRefreshTimer, bossLastRefresh, bossNextRefresh });
					bossdate = null;
					break;
				}
				bossdate = null;
			}
		}
	}

	@Override
	public int getCount() {
		return bossList.size();
	}

	@Override
	public Object getItem(int position) {
		return bossList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return bossList.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view;
		ViewHolder holder;
		if (convertView == null) {
			view = View.inflate(context.getApplicationContext(), R.layout.bosstime_item, null);

			holder = new ViewHolder();
			holder.iv_bossHead = (ImageView) view.findViewById(R.id.iv_bossHead);
			holder.tv_bosName = (TextView) view.findViewById(R.id.tv_bosName);
			holder.tv_bossAdd = (TextView) view.findViewById(R.id.tv_bossAdd);
			holder.tv_bossTime = (TextView) view.findViewById(R.id.tv_bossTime);
			holder.tv_bossTime_last = (TextView) view.findViewById(R.id.tv_bossTime_last);
			holder.tv_bossTime_next = (TextView) view.findViewById(R.id.tv_bossTime_next);
			view.setTag(holder);

		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}
		
		if (cacheBossTimeList != null && cacheBossTimeList.size() > 0) {
			holder.tv_bossTime.setText(cacheBossTimeList.get(position)[0]);
			holder.tv_bossTime_last.setText("上次次刷新" + cacheBossTimeList.get(position)[1]);
			holder.tv_bossTime_next.setText("下次刷新" + cacheBossTimeList.get(position)[2]);
		}
		
		if(position<=13){
			holder.iv_bossHead.setImageResource(bossImgs[position]);
		}

		holder.tv_bosName.setText(bossList.get(position).getC_name() + " " + bossList.get(position).getLevel());
		holder.tv_bossAdd.setText(bossList.get(position).getWaypoint().getName());

		return view;
	}

	class ViewHolder {
		ImageView iv_bossHead;
		TextView tv_bosName;
		TextView tv_bossAdd;
		TextView tv_bossTime;
		TextView tv_bossTime_last;
		TextView tv_bossTime_next;
	}

}
