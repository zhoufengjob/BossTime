package com.yaoby.gw2.bosstimer.adapter;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.yaoby.gw2.bosstimer.ui.fragments.*;

public class ContentPagerAdapter extends FragmentPagerAdapter {
	private ArrayList<Fragment> fragments ;

	public ContentPagerAdapter(FragmentManager fm) {
		super(fm);
		fragments = new ArrayList<Fragment>();
		
		fragments.add(new BossTimerFragment_());
		fragments.add(new WVWDataFragment_());
		fragments.add(new WVWRankingList_());
//		fragments.add(new BossTimerFragment_());
//		fragments.add(new BossTimerFragment_());
	}

	@Override
	public Fragment getItem(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}
	
}