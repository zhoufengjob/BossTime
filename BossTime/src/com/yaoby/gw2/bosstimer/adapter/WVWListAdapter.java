package com.yaoby.gw2.bosstimer.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.PieGraph;
import com.echo.holographlibrary.PieSlice;
import com.yaoby.gw2.bosstimer.MyContext.WorldServerEnum;
import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.response.WVWDataResponse;
import com.yaoby.gw2.bosstimer.response.wvw.Battlefield;
import com.yaoby.gw2.bosstimer.response.wvw.World;

public class WVWListAdapter extends BaseAdapter {

	private ArrayList<Battlefield> battlefieldList;
	private Context context;
	private ArrayList<String[]> cacheBossTimeList;

	public WVWListAdapter(Context context, WVWDataResponse wvwlistResponse, WorldServerEnum worldServerEnum) {
		
		this.context = context;
		
		//得到服务器大区数据列表
		if(worldServerEnum==WorldServerEnum.CHINA_TELECOM_1){
			this.battlefieldList = wvwlistResponse.getServer().getChina_telecom_1().getList().getBattlefieldList();
		}else if (worldServerEnum==WorldServerEnum.CHINA_TELECOM_2) {
			this.battlefieldList = wvwlistResponse.getServer().getChina_telecom_2().getList().getBattlefieldList();
		}else if (worldServerEnum==WorldServerEnum.CHINA_UNICOM_1) {
			this.battlefieldList = wvwlistResponse.getServer().getChina_unicom_1().getList().getBattlefieldList();
		}
	}


	@Override
	public int getCount() {
		return battlefieldList.size();
	}

	@Override
	public Object getItem(int position) {
		return battlefieldList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view;
		ViewHolder holder;
		if (convertView == null) {
			view = View.inflate(context.getApplicationContext(), R.layout.wvw_battlefield_item, null);

			holder = new ViewHolder();
			holder.tv_red_server_name = (TextView) view.findViewById(R.id.tv_red_server_name);
			holder.tv_red_server_income = (TextView) view.findViewById(R.id.tv_red_server_income);
			holder.tv_red_server_scores = (TextView) view.findViewById(R.id.tv_red_server_scores);
			
			holder.tv_blue_server_name = (TextView) view.findViewById(R.id.tv_blue_server_name);
			holder.tv_blue_server_income = (TextView) view.findViewById(R.id.tv_blue_server_income);
			holder.tv_blue_server_scores = (TextView) view.findViewById(R.id.tv_blue_server_scores);
			
			holder.tv_green_server_name = (TextView) view.findViewById(R.id.tv_green_server_name);
			holder.tv_green_server_income = (TextView) view.findViewById(R.id.tv_green_server_income);
			holder.tv_green_server_scores = (TextView) view.findViewById(R.id.tv_green_server_scores);
			
			holder.graph = (PieGraph) view.findViewById(R.id.graph);
			
			holder.bar_graph = (BarGraph) view.findViewById(R.id.bar_graph);

			view.setTag(holder);

		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}
		
		World redWorld = battlefieldList.get(position).getRedWorld();
		World blueWorld = battlefieldList.get(position).getBlueWorld();
		World greenWorld = battlefieldList.get(position).getGreenWorld();
		
		
		//填充数据
		//服务器名称
		holder.tv_red_server_name.setText(redWorld.getName());
		holder.tv_blue_server_name.setText(blueWorld.getName());
		holder.tv_green_server_name.setText(greenWorld.getName());
		
		//潜力分（每10分结算）
		holder.tv_red_server_income.setText(redWorld.getIncome().toString());
		holder.tv_blue_server_income.setText(blueWorld.getIncome().toString());
		holder.tv_green_server_income.setText(greenWorld.getIncome().toString());
		
		//当前星期总得分
		holder.tv_red_server_scores.setText(redWorld.getScores());
		holder.tv_blue_server_scores.setText(blueWorld.getScores());
		holder.tv_green_server_scores.setText(greenWorld.getScores());
		
//		//饼状图
//		PieSlice slice = new PieSlice();
//		slice.setColor(Color.parseColor("#99CC00"));
//		slice.setValue(Float.parseFloat(battlefieldList.get(position).getRedWorld().getIncome().toString()));
//		holder.graph.addSlice(slice);
//		
//		slice = new PieSlice();
//		slice.setColor(Color.parseColor("#FFBB33"));
//		slice.setValue(Float.parseFloat(battlefieldList.get(position).getBlueWorld().getIncome().toString()));
//		holder.graph.addSlice(slice);
//		
//		slice = new PieSlice();
//		slice.setColor(Color.parseColor("#AA66CC"));
//		slice.setValue(Float.parseFloat(battlefieldList.get(position).getGreenWorld().getIncome().toString()));
//		holder.graph.addSlice(slice);

		//桶状图
		ArrayList<Bar> points = new ArrayList<Bar>();
		Bar d = new Bar();
		d.setColor(Color.parseColor("#CE4841"));
		d.setName(redWorld.getName());
		//d.setValue(Float.parseFloat(redWorld.getScores()));
		d.setValue(Float.parseFloat(redWorld.getIncome().toString()));
		
		
		Bar d2 = new Bar();
		d2.setColor(Color.parseColor("#4777CD"));
		d2.setName(blueWorld.getName());
		d2.setValue(Float.parseFloat(blueWorld.getIncome().toString()));
		
		Bar d3 = new Bar();
		d3.setColor(Color.parseColor("#258828"));
		d3.setName(greenWorld.getName());
		d3.setValue(Float.parseFloat(greenWorld.getIncome().toString()));
		
		points.add(d);
		points.add(d2);
		points.add(d3);

		holder.bar_graph.setBars(points);
		holder.bar_graph.setUnit("得分：");
		
		
		
		return view;
	}

	class ViewHolder {
		PieGraph graph;
		BarGraph bar_graph;
		
		TextView 
		tv_red_server_name,
		tv_red_server_income,
		tv_red_server_scores,
		
		tv_blue_server_name,
		tv_blue_server_income,
		tv_blue_server_scores,
		
		tv_green_server_name,
		tv_green_server_income,
		tv_green_server_scores
		;
	}

}
