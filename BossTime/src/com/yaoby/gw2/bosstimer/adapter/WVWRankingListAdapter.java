package com.yaoby.gw2.bosstimer.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;//

import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.adapter.WVWListAdapter.ViewHolder;
import com.yaoby.gw2.bosstimer.response.Ranking;

public class WVWRankingListAdapter extends BaseAdapter {

	Context context;
	List<Ranking> list;

	public WVWRankingListAdapter(Context context, List<Ranking> list) {
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view;
		ViewHolder holder;
		if (convertView == null) {
			view = View.inflate(context.getApplicationContext(), R.layout.wvw_rankinglist_item, null);
			holder = new ViewHolder();

			holder.tv_rank = (TextView) view.findViewById(R.id.tv_rank);
			holder.tv_shardname = (TextView) view.findViewById(R.id.tv_shardname);
			holder.tv_integral = (TextView) view.findViewById(R.id.tv_integral);
			holder.tv_successes = (TextView) view.findViewById(R.id.tv_successes);
			holder.tv_compare = (TextView) view.findViewById(R.id.tv_compare);
			
			view.setTag(holder);
		} else {
			view = convertView;
			holder = (ViewHolder) view.getTag();
		}

		holder.tv_rank.setText(list.get(position).getRank() + "");
		holder.tv_shardname.setText(list.get(position).getShardname()+"");
		holder.tv_integral.setText(list.get(position).getIntegral()+"");

		holder.tv_successes.setText(list.get(position).getFirstplacecounts()+ "/"+list.get(position).getSecondplacecounts() + "/" + list.get(position).getThirdplacecounts());

		if (list.get(position).getComparestatus() < 0) {
			holder.tv_compare.setText("下降" + Math.abs(list.get(position).getComparestatus()) + "位");
			holder.tv_compare.setTextColor(Color.parseColor("#CE4841"));
		}
		
		if (list.get(position).getComparestatus() > 0) {
			holder.tv_compare.setText("上升" + Math.abs(list.get(position).getComparestatus()) + "位");
			holder.tv_compare.setTextColor(Color.parseColor("#258828"));
		}
		
		if(list.get(position).getComparestatus() == 0){
			holder.tv_compare.setText("无变化");
			holder.tv_compare.setTextColor(Color.parseColor("#FFFFFF"));
		}
		return view;
	}

	class ViewHolder {

		TextView tv_rank, tv_shardname, tv_integral, tv_successes, tv_compare;
	}

}
