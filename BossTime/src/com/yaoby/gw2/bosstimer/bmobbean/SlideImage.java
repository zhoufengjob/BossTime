package com.yaoby.gw2.bosstimer.bmobbean;

import cn.bmob.v3.BmobObject;
/**
 * 幻灯片
 * @author zhoufeng
 *
 */
public class SlideImage extends BmobObject{
	String url;			//URL
	String describe;	//描述
	String redirectURL;	//重定向链接
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getRedirectURL() {
		return redirectURL;
	}
	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}
	
	
}
