package com.yaoby.gw2.bosstimer.response;

import com.yaoby.gw2.bosstimer.response.bosstime.BossTime;


public class BossTimeResponse {

//	ArrayList<BossTime> bossList;
//
//	public ArrayList<BossTime> getBosslist() {
//		return bossList;
//	}
//
//	public void setBosslist(ArrayList<BossTime> bossList) {
//		this.bossList = bossList;
//	}
	
	BossTime boss1;
	BossTime boss2;
	BossTime boss3;
	BossTime boss4;
	BossTime boss5;
	BossTime boss6;
	BossTime boss7;
	BossTime boss8;
	BossTime boss9;
	BossTime boss10;
	BossTime boss11;
	BossTime boss12;
	BossTime boss13;
	
	
	
	public BossTime getBoss1() {
		return boss1;
	}
	public void setBoss1(BossTime boss1) {
		this.boss1 = boss1;
	}
	public BossTime getBoss2() {
		return boss2;
	}
	public void setBoss2(BossTime boss2) {
		this.boss2 = boss2;
	}
	public BossTime getBoss3() {
		return boss3;
	}
	public void setBoss3(BossTime boss3) {
		this.boss3 = boss3;
	}
	public BossTime getBoss4() {
		return boss4;
	}
	public void setBoss4(BossTime boss4) {
		this.boss4 = boss4;
	}
	public BossTime getBoss5() {
		return boss5;
	}
	public void setBoss5(BossTime boss5) {
		this.boss5 = boss5;
	}
	public BossTime getBoss6() {
		return boss6;
	}
	public void setBoss6(BossTime boss6) {
		this.boss6 = boss6;
	}
	public BossTime getBoss7() {
		return boss7;
	}
	public void setBoss7(BossTime boss7) {
		this.boss7 = boss7;
	}
	public BossTime getBoss8() {
		return boss8;
	}
	public void setBoss8(BossTime boss8) {
		this.boss8 = boss8;
	}
	public BossTime getBoss9() {
		return boss9;
	}
	public void setBoss9(BossTime boss9) {
		this.boss9 = boss9;
	}
	public BossTime getBoss10() {
		return boss10;
	}
	public void setBoss10(BossTime boss10) {
		this.boss10 = boss10;
	}
	public BossTime getBoss11() {
		return boss11;
	}
	public void setBoss11(BossTime boss11) {
		this.boss11 = boss11;
	}
	public BossTime getBoss12() {
		return boss12;
	}
	public void setBoss12(BossTime boss12) {
		this.boss12 = boss12;
	}
	public BossTime getBoss13() {
		return boss13;
	}
	public void setBoss13(BossTime boss13) {
		this.boss13 = boss13;
	}

}
