package com.yaoby.gw2.bosstimer.response;

public class Ranking {
	int sessionid ;
	String shardname;
	int integral;
	int firstplacecounts;
	int secondplacecounts;
	int thirdplacecounts;
	String color;
	int score;
	int comparestatus;
	int rank;
	String updatetime;
	int integralScore;
	String areaname;
	
	
	public int getSessionid() {
		return sessionid;
	}
	public void setSessionid(int sessionid) {
		this.sessionid = sessionid;
	}
	public String getShardname() {
		return shardname;
	}
	public void setShardname(String shardname) {
		this.shardname = shardname;
	}
	public int getIntegral() {
		return integral;
	}
	public void setIntegral(int integral) {
		this.integral = integral;
	}
	public int getFirstplacecounts() {
		return firstplacecounts;
	}
	public void setFirstplacecounts(int firstplacecounts) {
		this.firstplacecounts = firstplacecounts;
	}
	public int getSecondplacecounts() {
		return secondplacecounts;
	}
	public void setSecondplacecounts(int secondplacecounts) {
		this.secondplacecounts = secondplacecounts;
	}
	public int getThirdplacecounts() {
		return thirdplacecounts;
	}
	public void setThirdplacecounts(int thirdplacecounts) {
		this.thirdplacecounts = thirdplacecounts;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getComparestatus() {
		return comparestatus;
	}
	public void setComparestatus(int comparestatus) {
		this.comparestatus = comparestatus;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	public int getIntegralScore() {
		return integralScore;
	}
	public void setIntegralScore(int integralScore) {
		this.integralScore = integralScore;
	}
	public String getAreaname() {
		return areaname;
	}
	public void setAreaname(String areaname) {
		this.areaname = areaname;
	}
	
	
}
