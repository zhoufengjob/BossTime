package com.yaoby.gw2.bosstimer.response;

import org.json.*;

import com.alibaba.fastjson.JSON;
import com.yaoby.gw2.bosstimer.response.wvw.WorldServer;


public class WVWDataResponse {
	
    private String matchNum;
    private String endTime;
    private String time;
    private WorldServer server;
    
	public WVWDataResponse () {
		
	}	
    
	public WVWDataResponse (String jsonStr) {
		this.matchNum = JSON.parseObject(jsonStr).getString("match_num");
		this.endTime = JSON.parseObject(jsonStr).getString("end_time");
		this.time = JSON.parseObject(jsonStr).getString("time");
		this.server = new WorldServer(JSON.parseObject(jsonStr).getString("server"));
	}	
    
    public String getMatchNum() {
        return this.matchNum;
    }

    public void setMatchNum(String matchNum) {
        this.matchNum = matchNum;
    }

    public String getEndTime() {
        return this.endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public WorldServer getServer() {
        return this.server;
    }

    public void setServer(WorldServer server) {
        this.server = server;
    }


    
}
