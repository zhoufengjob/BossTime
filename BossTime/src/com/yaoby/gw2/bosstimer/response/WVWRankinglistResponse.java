package com.yaoby.gw2.bosstimer.response;

import java.util.List;

import com.alibaba.fastjson.JSON;

public class WVWRankinglistResponse {

	List<Ranking> china_telecom_1;
	List<Ranking> china_telecom_2;
	List<Ranking> china_unicom_1;

	private String china_telecom_1_jsonStr, china_telecom_2_jsonStr, china_unicom_1_jsonStr;

	public WVWRankinglistResponse() {

	}

	public WVWRankinglistResponse(String jsonStr) {
		china_telecom_1_jsonStr = JSON.parseObject(jsonStr).getString("电信一区");
		china_telecom_2_jsonStr = JSON.parseObject(jsonStr).getString("电信二区");
		china_unicom_1_jsonStr = JSON.parseObject(jsonStr).getString("联通一区");

		this.china_telecom_1 = JSON.parseArray(china_telecom_1_jsonStr, Ranking.class);
		this.china_telecom_2 = JSON.parseArray(china_telecom_2_jsonStr, Ranking.class);
		this.china_unicom_1 = JSON.parseArray(china_unicom_1_jsonStr, Ranking.class);

	}

	public List<Ranking> getChina_telecom_1() {
		return china_telecom_1;
	}

	public void setChina_telecom_1(List<Ranking> china_telecom_1) {
		this.china_telecom_1 = china_telecom_1;
	}

	public List<Ranking> getChina_telecom_2() {
		return china_telecom_2;
	}

	public void setChina_telecom_2(List<Ranking> china_telecom_2) {
		this.china_telecom_2 = china_telecom_2;
	}

	public List<Ranking> getChina_unicom_1() {
		return china_unicom_1;
	}

	public void setChina_unicom_1(List<Ranking> china_unicom_1) {
		this.china_unicom_1 = china_unicom_1;
	}

	public String getChina_telecom_1_jsonStr() {
		return china_telecom_1_jsonStr;
	}

	public void setChina_telecom_1_jsonStr(String china_telecom_1_jsonStr) {
		this.china_telecom_1_jsonStr = china_telecom_1_jsonStr;
	}

	public String getChina_telecom_2_jsonStr() {
		return china_telecom_2_jsonStr;
	}

	public void setChina_telecom_2_jsonStr(String china_telecom_2_jsonStr) {
		this.china_telecom_2_jsonStr = china_telecom_2_jsonStr;
	}

	public String getChina_unicom_1_jsonStr() {
		return china_unicom_1_jsonStr;
	}

	public void setChina_unicom_1_jsonStr(String china_unicom_1_jsonStr) {
		this.china_unicom_1_jsonStr = china_unicom_1_jsonStr;
	}

}
