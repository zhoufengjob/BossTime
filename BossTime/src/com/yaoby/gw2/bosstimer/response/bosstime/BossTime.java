package com.yaoby.gw2.bosstimer.response.bosstime;

import java.io.Serializable;
import java.util.ArrayList;

public class BossTime implements Serializable{
	
    private int id;
    private String c_name;
    private String name;
    private ArrayList<String> time;
    private String level;
    private Waypoint waypoint;
    
    
    
    
    public String getC_name() {
		return c_name;
	}

	public void setC_name(String c_name) {
		this.c_name = c_name;
	}

	public Waypoint getWaypoint() {
		return waypoint;
	}

	public void setWaypoint(Waypoint waypoint) {
		this.waypoint = waypoint;
	}

	public BossTime() {
	}
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getc_name() {
		return c_name;
	}
	public void setcName(String c_name) {
		this.c_name = c_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<String> getTime() {
		return time;
	}
	public void setTime(ArrayList<String> time) {
		this.time = time;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
    
    


    
}
