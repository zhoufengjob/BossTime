package com.yaoby.gw2.bosstimer.response.bosstime;

import java.io.Serializable;
import java.util.ArrayList;

public class Waypoint implements Serializable{
	private String name ;
	private ArrayList<Double> coord;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Double> getCoord() {
		return coord;
	}
	public void setCoord(ArrayList<Double> coord) {
		this.coord = coord;
	}
	
	
}
