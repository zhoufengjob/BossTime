package com.yaoby.gw2.bosstimer.response.wvw;

import com.alibaba.fastjson.JSON;

public class Battlefield {

	private World greenWorld;
	private World redWorld;
	private World blueWorld;
	private String rank;

	public Battlefield() {
	}
	
	public Battlefield(String jsonStr) {
		this.greenWorld = new World(JSON.parseObject(jsonStr).getString("green_world"));
		this.redWorld = new World(JSON.parseObject(jsonStr).getString("red_world"));
		this.blueWorld = new World(JSON.parseObject(jsonStr).getString("blue_world"));
		this.rank = JSON.parseObject(jsonStr).getString("rank");
	}

	public World getGreenWorld() {
		return greenWorld;
	}

	public void setGreenWorld(World greenWorld) {
		this.greenWorld = greenWorld;
	}

	public World getRedWorld() {
		return redWorld;
	}

	public void setRedWorld(World redWorld) {
		this.redWorld = redWorld;
	}

	public World getBlueWorld() {
		return blueWorld;
	}

	public void setBlueWorld(World blueWorld) {
		this.blueWorld = blueWorld;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

}
