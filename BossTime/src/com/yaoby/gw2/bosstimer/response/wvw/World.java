package com.yaoby.gw2.bosstimer.response.wvw;

import com.alibaba.fastjson.JSON;

public class World {

	private String scores; // 得分
	private double income; // 收益
	private double castle; // 城堡
	private double tower; // 塔楼
	private double camp; // 营地
	private double keep;//要塞
	private String name;
	

	public World(String jsonStr) {
		this.scores = JSON.parseObject(jsonStr).getString("scores");
		this.income = JSON.parseObject(jsonStr).getDouble("income");
		this.castle = JSON.parseObject(jsonStr).getDouble("castle");
		this.tower = JSON.parseObject(jsonStr).getDouble("tower");
		this.camp = JSON.parseObject(jsonStr).getDouble("camp");
		this.keep = JSON.parseObject(jsonStr).getDouble("keep");
		this.name = JSON.parseObject(jsonStr).getString("name");
	}

	public String getScores() {
		return scores;
	}

	public void setScores(String scores) {
		this.scores = scores;
	}

	public Double getIncome() {
		return income;
	}

	public void setIncome(Double income) {
		this.income = income;
	}

	public Double getCastle() {
		return castle;
	}

	public void setCastle(Double castle) {
		this.castle = castle;
	}

	public Double getTower() {
		return tower;
	}

	public void setTower(Double tower) {
		this.tower = tower;
	}

	public Double getCamp() {
		return camp;
	}

	public void setCamp(Double camp) {
		this.camp = camp;
	}

	public Double getKeep() {
		return keep;
	}

	public void setKeep(Double keep) {
		this.keep = keep;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
