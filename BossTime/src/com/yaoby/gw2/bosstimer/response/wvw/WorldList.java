package com.yaoby.gw2.bosstimer.response.wvw;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.yaoby.gw2.bosstimer.MyContext;

public class WorldList implements MyContext {

	//电信一区服务器战场
	private ArrayList<Battlefield> battlefieldList ;
	
	
	public WorldList() {
		
	}

	public WorldList(String jsonStr, WorldServerEnum server) {
		battlefieldList = null;
		battlefieldList = new ArrayList<Battlefield>();
		
		if (server == WorldServerEnum.CHINA_TELECOM_1) {
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("7-1")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("7-2")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("7-3")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("7-4")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("7-5")));
		} else if (server == WorldServerEnum.CHINA_TELECOM_2) {
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("8-1")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("8-2")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("8-3")));
		} else if (server == WorldServerEnum.CHINA_UNICOM_1) {
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("9-1")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("9-2")));
			battlefieldList.add( new Battlefield(JSON.parseObject(jsonStr).getString("9-3")));
			
		}

	}

	public ArrayList<Battlefield> getBattlefieldList() {
		return battlefieldList;
	}

	public void setBattlefieldList(ArrayList<Battlefield> battlefieldList) {
		this.battlefieldList = battlefieldList;
	}


	
	

}
