package com.yaoby.gw2.bosstimer.response.wvw;

import com.alibaba.fastjson.JSON;
import com.yaoby.gw2.bosstimer.MyContext;
import com.yaoby.gw2.bosstimer.MyContext.WorldServerEnum;


public class WorldNetLine {
	
    private String name;
    private WorldList worldList;
    
    
	public WorldNetLine (String  jsonStr, WorldServerEnum server) {
		this.name = JSON.parseObject(jsonStr).getString("name");
		this.worldList = new WorldList(JSON.parseObject(jsonStr).getString("list"),server);
	}	
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorldList getList() {
        return this.worldList;
    }

    public void setList(WorldList list) {
        this.worldList = list;
    }


    
}
