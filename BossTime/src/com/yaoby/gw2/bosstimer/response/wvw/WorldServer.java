package com.yaoby.gw2.bosstimer.response.wvw;

import com.alibaba.fastjson.JSON;
import com.yaoby.gw2.bosstimer.MyContext;

public class WorldServer implements MyContext {

	private WorldNetLine china_telecom_1;
	private WorldNetLine china_telecom_2;
	private WorldNetLine china_unicom_1;

	public WorldServer() {
	}

	public WorldServer(String jsonStr) {
		this.china_telecom_1 = new WorldNetLine(JSON.parseObject(jsonStr).getString("电信一区"), WorldServerEnum.CHINA_TELECOM_1);
		this.china_telecom_2 = new WorldNetLine(JSON.parseObject(jsonStr).getString("电信二区"), WorldServerEnum.CHINA_TELECOM_2);
		this.china_unicom_1 = new WorldNetLine(JSON.parseObject(jsonStr).getString("联通一区"), WorldServerEnum.CHINA_UNICOM_1);
	}

	public WorldNetLine getChina_telecom_1() {
		return china_telecom_1;
	}

	public void setChina_telecom_1(WorldNetLine china_telecom_1) {
		this.china_telecom_1 = china_telecom_1;
	}

	public WorldNetLine getChina_telecom_2() {
		return china_telecom_2;
	}

	public void setChina_telecom_2(WorldNetLine china_telecom_2) {
		this.china_telecom_2 = china_telecom_2;
	}

	public WorldNetLine getChina_unicom_1() {
		return china_unicom_1;
	}

	public void setChina_unicom_1(WorldNetLine china_unicom_1) {
		this.china_unicom_1 = china_unicom_1;
	}

}
