package com.yaoby.gw2.bosstimer.ui;

import java.util.Date;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.adapter.ContentPagerAdapter;
import com.yaoby.gw2.bosstimer.ui.fragments.*;
import com.yaoby.gw2.bosstimer.utils.DateTimeUtil;

@EActivity(R.layout.activity_main)
public class MainActivity extends FragmentActivity {
	@ViewById
	ViewPager vp_content;
	@ViewById
	TextView tv_top_functionList, tv_top_bossTime,tv_top_wvw;
	@ViewById
	SlidingMenu menu_frame;
	@ViewById
	ImageButton ibtn_right_menu;

	private ImageView cursor;// 动画图片
	private int bmpW;// 动画图片宽度
	private int offset = 0;// 动画图片偏移量
	private int currIndex = 0;// 当前页卡编号

	@AfterViews
	void init() {
		tv_top_functionList.setTextColor(getResources().getColor(R.color.grey));
		tv_top_bossTime.setTextColor(getResources().getColor(R.color.darker_red));
		tv_top_wvw.setTextColor(getResources().getColor(R.color.grey));
		
		vp_content.setAdapter(new ContentPagerAdapter(getSupportFragmentManager()));
		vp_content.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				setCurrentPage(position);
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				// ignore
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// ignore
			}
		});

		menu_frame = new SlidingMenu(this);
		menu_frame.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE); // 滑动方式
		menu_frame.setShadowDrawable(R.drawable.shadow_right); // 阴影
		menu_frame.setShadowWidth(30); // 阴影宽度
		menu_frame.setBehindOffset(80); // 前面的视图剩下多少
		menu_frame.setMode(SlidingMenu.RIGHT); // 左滑出不是右滑出
		menu_frame.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu_frame.setMenu(R.layout.menu_frame); // 设置menu容器
		FragmentManager fm = getSupportFragmentManager();
		fm.beginTransaction().replace(R.id.menu_frame, new MenuFragment_()).commit();
 
		InitImageView();
	}

	@Click(R.id.ibtn_right_menu)
	void ibtn_right_menu_Click() {
		menu_frame.toggle();
	}
	
	@Click(R.id.tv_top_functionList)
	void tv_top_functionList_Click(){
		vp_content.setCurrentItem(2);
	}
	
	@Click(R.id.tv_top_bossTime)
	void tv_top_bossTime_Click(){
		vp_content.setCurrentItem(0);
	}
	
	@Click(R.id.tv_top_wvw)
	void tv_top_wvw_Click(){
		vp_content.setCurrentItem(1);
	}
	
	
	// 按下返回键时
	@Override
	public void onBackPressed() {
		if (menu_frame != null && menu_frame.isMenuShowing()) {
			menu_frame.showContent();
		} else {
			super.onBackPressed();
		}
	}

	// 按下菜单键时
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			menu_frame.toggle();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 初始化动画
	 */
	private void InitImageView() {
		cursor = (ImageView) findViewById(R.id.cursor);
		bmpW = BitmapFactory.decodeResource(getResources(), R.drawable.a).getWidth();// 获取图片宽度
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int screenW = dm.widthPixels;// 获取分辨率宽度
		offset = (screenW / 3 - bmpW) / 2;// 计算偏移量
		Matrix matrix = new Matrix();
		matrix.postTranslate(offset, 0);
		cursor.setImageMatrix(matrix);// 设置动画初始位置
	}

	/**
	 * 翻页逻辑
	 * 
	 * @param current
	 */
	private void setCurrentPage(int current) {
		int one = offset * 2 + bmpW;// 页卡1 -> 页卡2 偏移量
		int two = one * 2;// 页卡1 -> 页卡3 偏移量
		Animation animation = null;

		if (current < 2) {
			menu_frame.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		} else {
			menu_frame.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		}

		switch (current) {
		case 0:
			tv_top_functionList.setTextColor(getResources().getColor(R.color.grey));
			tv_top_bossTime.setTextColor(getResources().getColor(R.color.darker_red));
			tv_top_wvw.setTextColor(getResources().getColor(R.color.grey));

			if (currIndex == 1) {
				animation = new TranslateAnimation(one, 0, 0, 0);
			} else if (currIndex == 2) {
				animation = new TranslateAnimation(two, 0, 0, 0);
			}
			
			//测试开始
	        
			String sss =DateTimeUtil.getDateTime("yyyy年MM月dd日") ;  
		    String test = "2003-1-31";  
		    Date date;  
		    try {  
		      date = DateTimeUtil.parse(test, "");  
		      Log.e("bosstime","sss="+sss);  
		  
		      Log.e("bosstime","得到当前日期 － getDate():" + DateTimeUtil.getDate());  
		      Log.e("bosstime","得到当前日期时间 － getDateTime():" + DateTimeUtil.getDateTime());  
		  
		      Log.e("bosstime","得到当前年份 － getCurrentYear():" + DateTimeUtil.getCurrentYear());  
		      Log.e("bosstime","得到当前月份 － getCurrentMonth():" + DateTimeUtil.getCurrentMonth());  
		      Log.e("bosstime","得到当前日子 － getCurrentDay():" + DateTimeUtil.getCurrentDay());  
		  
		      Log.e("bosstime","解析 － parse(" + test + "):" + DateTimeUtil.getDateTime(date, "yyyy-MM-dd"));   
		  
		      Log.e("bosstime","自增月份 － addMonths(3):" + DateTimeUtil.getDateTime(DateTimeUtil.addMonths(3), "yyyy-MM-dd"));  
		      Log.e("bosstime","增加月份 － addMonths(" + test + ",3):" + DateTimeUtil.getDateTime(DateTimeUtil.addMonths(date, 3), "yyyy-MM-dd"));  
		      Log.e("bosstime","增加日期 － addDays(" + test + ",3):" + DateTimeUtil.getDateTime(DateTimeUtil.addDays(date, 3), "yyyy-MM-dd"));  
		      Log.e("bosstime","自增日期 － addDays(3):" + DateTimeUtil.getDateTime(DateTimeUtil.addDays(3), "yyyy-MM-dd"));  
		  
		      Log.e("bosstime","比较日期 － diffDays():" + DateTimeUtil.diffDays(DateTimeUtil.getNow(), date));  
		      Log.e("bosstime","比较月份 － diffMonths():" + DateTimeUtil.diffMonths(DateTimeUtil.getNow(), date));  
		  
		      Log.e("bosstime","得到" + test + "所在月份的最后一天:" + DateTimeUtil.getDateTime(DateTimeUtil.getMonthLastDay(date), "yyyy-MM-dd"));  
		  
		    }catch (Exception e) {  
		      Log.e("bosstime",e.getStackTrace()+"");  
		    }  
		    
		    //测试结束

			break;
		case 1:
			tv_top_bossTime.setTextColor(getResources().getColor(R.color.grey));
			tv_top_functionList.setTextColor(getResources().getColor(R.color.grey));
			tv_top_wvw.setTextColor(getResources().getColor(R.color.darker_red));

			if (currIndex == 0) {
				animation = new TranslateAnimation(offset, one, 0, 0);
			} else if (currIndex == 2) {
				animation = new TranslateAnimation(two, one, 0, 0);
			}
			break;
		case 2:
			tv_top_functionList.setTextColor(getResources().getColor(R.color.darker_red));
			tv_top_wvw.setTextColor(getResources().getColor(R.color.grey));
			tv_top_bossTime.setTextColor(getResources().getColor(R.color.grey));
			

			if (currIndex == 0) {
				animation = new TranslateAnimation(offset, two, 0, 0);
			} else if (currIndex == 1) {
				animation = new TranslateAnimation(one, two, 0, 0);
			}

			break;
		}

		currIndex = current;
		animation.setFillAfter(true);// True:图片停在动画结束位置
		animation.setDuration(300);
		cursor.startAnimation(animation);

	}
	
	
}