package com.yaoby.gw2.bosstimer.ui;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.yaoby.gw2.bosstimer.R;

@EActivity(R.layout.start)
public class StartActivity extends BaseActivity{ 

	static int minute = -1;
	static int second = -1;
	TextView timeView;
	Timer timer;
	TimerTask  timerTask;
	
	
	Handler handler = new Handler(){
		public void handleMessage(Message msg) {
			
			if (minute == 0) {
				if (second == 0) {
					timeView.setText("Time out !");
					if (timer != null) {
						timer.cancel();
						timer = null;
					}
					if (timerTask != null) {
						timerTask = null;
					}
				}else {
					second--;
					if (second >= 10) {
						timeView.setText("0"+minute + ":" + second);
					}else {
						timeView.setText("0"+minute + ":0" + second);
					}
				}
			}else {
				if (second == 0) {
					second =59;
					minute--;
					if (minute >= 10) {
						timeView.setText(minute + ":" + second);
					}else {
						timeView.setText("0"+minute + ":" + second);
					}
				}else {
					second--;
					if (second >= 10) {
						if (minute >= 10) {
							timeView.setText(minute + ":" + second);
						}else {
							timeView.setText("0"+minute + ":" + second);
						}
					}else {
						if (minute >= 10) {
							timeView.setText(minute + ":0" + second);
						}else {
							timeView.setText("0"+minute + ":0" + second);
						}
					}
				}
			}
			
			
		};
	};
	
	@AfterViews
	void logic(){
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		timeView = (TextView)findViewById(R.id.myTime);
		
		if (minute == -1 && second == -1) {
			Intent intent = getIntent();
			ArrayList<Integer> times = intent.getIntegerArrayListExtra("times");
			minute = times.get(0);
			second = times.get(1);
		}
		
		timeView.setText(minute + ":" + second);
		
		timerTask = new TimerTask() {
			
			@Override
			public void run() {
				Message msg = new Message();
				msg.what = 0;
				handler.sendMessage(msg);
			}
		};
		
		timer = new Timer();
		timer.schedule(timerTask,0,1000);
	}
	
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		if (timerTask != null) {
			timerTask = null;
		}
		minute = -1;
		second = -1;
	}
	
	
}
