package com.yaoby.gw2.bosstimer.ui.fragments;

import org.androidannotations.annotations.EFragment;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

@EFragment
public abstract class BaseFragment extends Fragment {
	/**
	 * Fragment注入后
	 */
	abstract void voidcalledAfterInjection();
	
	
	/**
	 * 控件注入后
	 */
	abstract void voidcalledAfterViewInjection();
	
	public void toast(String msg){
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
		Log.d("bosstime", msg);
	}
}