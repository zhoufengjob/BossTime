package com.yaoby.gw2.bosstimer.ui.fragments;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import uk.co.senab.actionbarpulltorefresh.library.viewdelegates.AbsListViewDelegate;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yaoby.gw2.bosstimer.AllURL;
import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.adapter.BossTimeAdapter;
import com.yaoby.gw2.bosstimer.response.BossTimeResponse;
import com.yaoby.gw2.bosstimer.response.bosstime.BossTime;

/**
 * Boss 计时器
 * 
 * @author zhoufeng
 * 
 */

@EFragment(R.layout.fragment_bosstimer)
public class BossTimerFragment extends BaseFragment implements OnRefreshListener{

	@ViewById
	TextView tv_boss1,tv_msg;
	
	@ViewById
	ListView lv_bossTimelist;
	
	@ViewById
	PullToRefreshLayout ptr_layout;
	
	int minute;
	int second;

	private int recLen = 11;
	private long hours;
	private long minutes;
	private long seconds;
	private long diff;
	private long days;
	
	private ArrayList<BossTime> bossList;
	private BossTimeAdapter bossTimeAdapter;
	AsyncHttpClient client = new AsyncHttpClient();

	@Override
	@AfterInject
	void voidcalledAfterInjection() {
		//Message message = handler.obtainMessage(1); // Message
		//handler.sendMessageDelayed(message, 1000);
	
		getTime();
	}

	@Override
	@AfterViews
	void voidcalledAfterViewInjection() {
		getData();
		if(bossTimeAdapter!=null){
			lv_bossTimelist.setAdapter(bossTimeAdapter);
		}
		
		 ActionBarPullToRefresh.from(getActivity()).listener(this)
         // Here we'll set a custom ViewDelegate
         .useViewDelegate(GridView.class, new AbsListViewDelegate())
         .setup(ptr_layout);
	}
	
	@Click(R.id.tv_boss1)
	void tv_boss1_click(){
		getData();
	}


	public static String getHtmlSource(String url) {
		StringBuffer stb = new StringBuffer();

		try {
			URLConnection uc = new URL(url).openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					uc.getInputStream(), "utf-8"));
			String temp = null;
			while ((temp = br.readLine()) != null) {
				stb.append(temp).append("\n");
			}
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return stb.toString();

	}

	/**
	 * 获取数据
	 */
	void getData() {
		client.get(AllURL.BossTimeURL, new AsyncHttpResponseHandler() {
			

			@Override
			public void onFailure(int statusCode, Throwable error, String content) {
				Toast.makeText(getActivity(), "获取数据超时，尝试下拉同步数据！", 0).show();
				ptr_layout.setRefreshComplete();
				
				
			};
			
			@Override
		    public void onSuccess(String responseJson) {
		    	
//		    	//本地
//				String responseJson = FileUtil.readJsonForAssets(getActivity(),"data.json");
//				BossTimeResponse bossTimeResponse = gson.fromJson(responseJson,BossTimeResponse.class);

				BossTimeResponse bossTimeResponse = JSON.parseObject(responseJson,BossTimeResponse.class);
				if(bossList==null){
					bossList = new ArrayList<BossTime>();
				}else {
					bossList.clear();
				}
				
				bossList.add(bossTimeResponse.getBoss1());
				bossList.add(bossTimeResponse.getBoss2());
				bossList.add(bossTimeResponse.getBoss3());
				bossList.add(bossTimeResponse.getBoss4());
				bossList.add(bossTimeResponse.getBoss5());
				bossList.add(bossTimeResponse.getBoss6());
				bossList.add(bossTimeResponse.getBoss7());
				bossList.add(bossTimeResponse.getBoss8());
				bossList.add(bossTimeResponse.getBoss9());
				bossList.add(bossTimeResponse.getBoss10());
				bossList.add(bossTimeResponse.getBoss11());
				bossList.add(bossTimeResponse.getBoss12());
				bossList.add(bossTimeResponse.getBoss13());
				
				if(bossTimeAdapter==null){
					bossTimeAdapter = new BossTimeAdapter(getActivity(),bossList);
				}
				
				if(bossList!=null){
					lv_bossTimelist.setAdapter(bossTimeAdapter);
				}
				
				ptr_layout.setRefreshComplete();
				tv_msg.setVisibility(View.GONE);
		    }
		});
		
	}


	Handler handler = new Handler() {

		public void handleMessage(Message msg) { // handle message
			switch (msg.what) {
			case 1:
				diff = diff - 1000;
				getShowTime();
				if (diff > 0) {
					Message message = handler.obtainMessage(1);
					handler.sendMessageDelayed(message, 1000);
				} else {
					tv_boss1.setVisibility(View.GONE);
				}
				break;
			default:
				break;
			}
			super.handleMessage(msg);
		}
	};

	/**
	 * 得到时间差
	 */
	private void getTime() {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String date = sDateFormat.format(new Date());
		System.out.println("现在时间：" + date);
		try {
			Date d1 = df.parse("2014-9-6 12:00:00");
			Date d2 = df.parse(date);
			diff = d1.getTime() - d2.getTime();// 这样得到的差值是微秒级别
			days = diff / (1000 * 60 * 60 * 24);
			hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
			minutes = (diff - days * (1000 * 60 * 60 * 24) - hours
					* (1000 * 60 * 60))
					/ (1000 * 60);
			seconds = (diff - days * (1000 * 60 * 60 * 24) - hours
					* (1000 * 60 * 60) - minutes * (1000 * 60)) / (1000);
			tv_boss1.setText("" + days + "天" + hours + "小时" + minutes + "分"
					+ seconds + "秒");
			System.out.println("现在时间：diff " + diff);
			System.out.println("" + days + "天" + hours + "小时" + minutes + "分"
					+ seconds + "秒");
		} catch (Exception e) {
		}
	}

	/**
	 * 获得要显示的时间
	 */
	public void getShowTime() {
		days = diff / (1000 * 60 * 60 * 24);
		hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
//		hours = diff / (1000 * 60 * 60);
		minutes = (diff - days * (1000 * 60 * 60 * 24) - hours
				* (1000 * 60 * 60))
				/ (1000 * 60);
		seconds = (diff - days * (1000 * 60 * 60 * 24) - hours
				* (1000 * 60 * 60) - minutes * (1000 * 60)) / (1000);
		 tv_boss1.setText("" + days + "天" + hours + "小时" + minutes + "分" +
		 seconds + "秒");

		//tv_boss1.setText(hours + "小时" + minutes + "分" + seconds + "秒");
	}


	@Override
	public void onRefreshStarted(View view) {
		getData();
	}
	
}