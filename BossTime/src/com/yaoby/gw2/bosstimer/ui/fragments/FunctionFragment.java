package com.yaoby.gw2.bosstimer.ui.fragments;

import java.util.HashMap;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.utils.TransformerAdapter;

@EFragment(R.layout.fragment_function)
public class FunctionFragment extends BaseFragment implements BaseSliderView.OnSliderClickListener {
	@ViewById
	SliderLayout sl_ider;
	@ViewById
	ListView transformers;
	
	@Override
	@AfterInject
	void voidcalledAfterViewInjection() {
	
	}

	@Override
	@AfterViews
	void voidcalledAfterInjection() {
		HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hannibal",R.drawable.hannibal);
        file_maps.put("Big Bang Theory",R.drawable.bigbang);
        file_maps.put("House of Cards",R.drawable.house);
        file_maps.put("Game of Thrones", R.drawable.game_of_thrones);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setOnSliderClickListener(this);
            //add your extra information
            textSliderView.getBundle()
                    .putString("extra",name);

            sl_ider.addSlider(textSliderView);
        }

        sl_ider.setPresetTransformer(SliderLayout.Transformer.Stack);//设置幻灯动画
        sl_ider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sl_ider.setCustomAnimation(new DescriptionAnimation());

        transformers.setAdapter(new TransformerAdapter(getActivity()));
        transformers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	sl_ider.setPresetTransformer(((TextView) view).getText().toString());
                Toast.makeText(getActivity(), ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
	}
	

	@Override
	public void onSliderClick(BaseSliderView slider) {
		 Toast.makeText(getActivity(),slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();		
	}
	
	
	
}