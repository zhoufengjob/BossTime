package com.yaoby.gw2.bosstimer.ui.fragments;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.util.Log;
import android.widget.TabHost;
import android.widget.Toast;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobDate;
import cn.bmob.v3.datatype.BmobGeoPoint;
import cn.bmob.v3.listener.CountListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.bmobbean.Person;
import com.yaoby.gw2.bosstimer.bmobbean.SlideImage;

@EFragment(R.layout.fragment_menu)
public class MenuFragment extends BaseFragment implements BaseSliderView.OnSliderClickListener{

	@ViewById
	SliderLayout sl_ider;
	
	BmobQuery<SlideImage> bmobQuery = new BmobQuery<SlideImage>();

	@Override
	@AfterViews
	void voidcalledAfterInjection() {
		//初始化图片
		iniImages(bmobQuery);
	}
	
	/**
	 * 获取图片地址
	 * @param bmobQuery
	 */
	private void iniImages(BmobQuery<SlideImage> bmobQuery) {
		//获取图片连接
		bmobQuery.findObjects(getActivity(), new FindListener<SlideImage>(){

			@Override
			public void onError(int arg0, String arg1) {
				Toast.makeText(getActivity(), "失败了", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess(List<SlideImage> objects) {
				HashMap<String, String> url_maps = new HashMap<String, String>();
				for (SlideImage img : objects) {
					url_maps.put(img.getDescribe(), img.getUrl());//加载图片地址
				}
				
				for (String name : url_maps.keySet()) {
					TextSliderView textSliderView = new TextSliderView(getActivity());
					// initialize a SliderLayout
					textSliderView.description(name).image(url_maps.get(name)).setOnSliderClickListener(MenuFragment.this);
					// add your extra information
					textSliderView.getBundle().putString("extra", name);

					sl_ider.addSlider(textSliderView);
				}

				sl_ider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);// 设置幻灯动画
				sl_ider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
				sl_ider.setCustomAnimation(new DescriptionAnimation());
			}
		} );
		
		
//		HashMap<String, String> url_maps = new HashMap<String, String>();
//		url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
//		url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
//		url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
//		url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");
//
//		HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
//		file_maps.put("Hannibal", R.drawable.hannibal);
//		file_maps.put("Big Bang Theory", R.drawable.bigbang);
//		file_maps.put("House of Cards", R.drawable.house);
//		file_maps.put("Game of Thrones", R.drawable.game_of_thrones);
	}

	@Override
	void voidcalledAfterViewInjection() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSliderClick(BaseSliderView slider) {
		Toast.makeText(getActivity(), slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
	}

}