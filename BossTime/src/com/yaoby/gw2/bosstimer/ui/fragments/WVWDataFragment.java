package com.yaoby.gw2.bosstimer.ui.fragments;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import uk.co.senab.actionbarpulltorefresh.library.viewdelegates.AbsListViewDelegate;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yaoby.gw2.bosstimer.AllURL;
import com.yaoby.gw2.bosstimer.MyContext;
import com.yaoby.gw2.bosstimer.R;
import com.yaoby.gw2.bosstimer.adapter.WVWListAdapter;
import com.yaoby.gw2.bosstimer.response.WVWDataResponse;

/**
 * WvW 实时数据
 * 
 * @author zhoufeng
 * 
 */

@EFragment(R.layout.fragment_wvw)
public class WVWDataFragment extends BaseFragment implements OnRefreshListener{

	@ViewById
	TextView tv_msg;
	
	@ViewById
	TabHost tabhost;
	
	@ViewById
	ListView lv_china_telecom_1,lv_china_telecom_2,lv_china_unicom_1;
	
	@ViewById
	PullToRefreshLayout ptr_layout1,ptr_layout2,ptr_layout3;
	
	AsyncHttpClient httpClient = new AsyncHttpClient();

	@Override
	@AfterInject
	void voidcalledAfterInjection() {
	}

	@Override
	@AfterViews
	void voidcalledAfterViewInjection() {
        
        View niTab = (View) LayoutInflater.from(getActivity()).inflate(R.layout.tabmini, null);  
        TextView text0 = (TextView) niTab.findViewById(R.id.tab_label);  
        text0.setText(MyContext.WorldServerEnum.CHINA_TELECOM_1.getName());  
          
        View woTab = (View) LayoutInflater.from(getActivity()).inflate(R.layout.tabmini, null);  
        TextView text1 = (TextView) woTab.findViewById(R.id.tab_label);  
        text1.setText(MyContext.WorldServerEnum.CHINA_TELECOM_2.getName());  
          
        View taTab = (View) LayoutInflater.from(getActivity()).inflate(R.layout.tabmini, null);  
        TextView text2 = (TextView) taTab.findViewById(R.id.tab_label);  
        text2.setText(MyContext.WorldServerEnum.CHINA_UNICOM_1.getName());  
          
//        View weTab = (View) LayoutInflater.from(getActivity()).inflate(R.layout.tabmini, null);  
//        TextView text3 = (TextView) weTab.findViewById(R.id.tab_label);  
//        text3.setText("we");  
        
        tabhost.setup();   //Call setup() before adding tabs if loading TabHost using findViewById().   
          
        tabhost.addTab(tabhost.newTabSpec("nitab").setIndicator(niTab).setContent(R.id.tab_china_telecom_1));  
        tabhost.addTab(tabhost.newTabSpec("wotab").setIndicator(woTab).setContent(R.id.tab_china_telecom_2));  
        tabhost.addTab(tabhost.newTabSpec("tatab").setIndicator(taTab).setContent(R.id.tab_china_unicom_1));  
        
        //异步请求数据
		getData();
		
		AbsListViewDelegate absListViewDelegate = new AbsListViewDelegate();
		
		//设置下拉刷新
		ActionBarPullToRefresh.from(getActivity()).listener(this)
        .useViewDelegate(GridView.class, absListViewDelegate)
        .setup(ptr_layout1);
		
		//设置下拉刷新
		ActionBarPullToRefresh.from(getActivity()).listener(this)
        .useViewDelegate(GridView.class,  absListViewDelegate)
        .setup(ptr_layout2);
		
		//设置下拉刷新
		ActionBarPullToRefresh.from(getActivity()).listener(this)
        .useViewDelegate(GridView.class, absListViewDelegate)
        .setup(ptr_layout3);
	}


	/**
	 * 获取数据
	 */
	void getData() {
		httpClient.get(AllURL.WVWListURL, new AsyncHttpResponseHandler() {
			

			@Override
			public void onFailure(int statusCode, Throwable error, String content) {
				Toast.makeText(getActivity(), R.string.get_the_data_timeout, Toast.LENGTH_SHORT).show();
				ptr_layout1.setRefreshComplete();
				ptr_layout2.setRefreshComplete();
				ptr_layout3.setRefreshComplete();
			};
			
			@Override
		    public void onSuccess(String responseJson) {
				WVWDataResponse wvwlistResponse = new WVWDataResponse(responseJson);
				wvwlistResponse.toString();
				ptr_layout1.setRefreshComplete();
				ptr_layout2.setRefreshComplete();
				ptr_layout3.setRefreshComplete();
				
				tv_msg.setVisibility(View.GONE);
				lv_china_telecom_1.setAdapter(new WVWListAdapter(getActivity(), wvwlistResponse,MyContext.WorldServerEnum.CHINA_TELECOM_1));
				lv_china_telecom_2.setAdapter(new WVWListAdapter(getActivity(), wvwlistResponse,MyContext.WorldServerEnum.CHINA_TELECOM_2));
				lv_china_unicom_1.setAdapter(new WVWListAdapter(getActivity(), wvwlistResponse,MyContext.WorldServerEnum.CHINA_UNICOM_1));
		    }
		});
		
	}


	@Override
	public void onRefreshStarted(View view) {
		getData();
	}
	
}